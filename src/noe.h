#ifndef _NMRGRAD_NOE_H_
#define _NMRGRAD_NOE_H_

#include <stdio.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <string.h>

#include "mol2/pdb.h"

#define __DMIN__ 1.0E-8

#define _PRINTF_ERROR(fmt, ...) do { \
    fprintf(stderr, "[Error] (%s: line %i) - " fmt "\n", __func__, __LINE__, ##__VA_ARGS__); \
    exit(EXIT_FAILURE); \
} while(0)

struct nmr_group // TODO: optimize npeaks
{
        int id;
        int size;
        int group[6];
};

struct nmr_group_list
{
        int ngroups;
        struct nmr_group *groups;

        int natoms;
        int *atoms;
};

struct nmr_grad
{
        int size;
        int natoms;
        int npeaks;
        double *gradx;
        double *grady;
        double *gradz;
};

struct nmr_workspace
{
        int size;
        gsl_vector *eval;
        gsl_matrix *evec;
        gsl_eigen_symmv_workspace *w;
        double *expeval;
        double *pks2;
        //struct mol_vector3* k_derivs;
};

struct nmr_noe
{
        int size;
        double omega;
        double t_cor;
        double t_mix;
        double cutoff;

        double *in;
        double *rx;
        double *r2;

        struct nmr_workspace *space;
        struct nmr_group_list *grps;

        double energy;
        double scale;
        double fit;
        double *exp;
        int *mask;

        struct nmr_grad *in_grad;
        struct nmr_grad *rx_grad;
};


struct nmr_noe *nmr_noe_create(struct nmr_group_list *grps,
                               double omega,
                               double t_cor,
                               double t_mix,
                               double cutoff);

void nmr_noe_init(struct nmr_noe *spec, struct nmr_group_list *grps,
                  double omega,
                  double t_cor,
                  double t_mix,
                  double cutoff);

void nmr_noe_destroy(struct nmr_noe *spec);

void nmr_noe_free(struct nmr_noe *spec);


struct nmr_workspace *nmr_workspace_create(int size);

void nmr_workspace_init(struct nmr_workspace *space, int size);

void nmr_workspace_destroy(struct nmr_workspace *space);

void nmr_workspace_free(struct nmr_workspace *space);


struct nmr_group_list *nmr_group_list_create(int ngroups, int natoms);

void nmr_group_list_init(struct nmr_group_list *space, int ngroups, int natoms);

void nmr_group_list_destroy(struct nmr_group_list *groups);

void nmr_group_list_free(struct nmr_group_list *groups);

struct nmr_group_list *nmr_group_list_read(char *path);

void nmr_group_list_write(const char *path, const struct nmr_group_list *groups);

void nmr_group_list_fwrite(FILE *f, const struct nmr_group_list *groups);


struct nmr_grad *nmr_grad_create(int natoms, int npeaks);

void nmr_grad_init(struct nmr_grad *grad, int natoms, int npeaks);

void nmr_grad_destroy(struct nmr_grad *grad);

void nmr_grad_free(struct nmr_grad *grad);


void nmr_matrix_fwrite(FILE *f, const double *m, const int size);

void nmr_matrix_fwrite_stacked(FILE *f, const double *m, const int size);

void nmr_matrix_write(const char *path, const double *m, const int size);

double *nmr_matrix_read(const char *path, const int size, int *mask);


void nmr_r2_mat(double *r2, const struct mol_atom_group *ag, const struct nmr_group_list *grps);

void nmr_rx_mat(struct nmr_noe *spect, const struct mol_atom_group *ag);

void nmr_compute_peaks(struct nmr_noe *spect, const struct mol_atom_group *ag);

void nmr_compute_peaks_no_grad(struct nmr_noe *spect, const struct mol_atom_group *ag);

void nmr_matrix_exponent(struct nmr_noe *spect);


int *get_mask(double *exp, double cutoff, int size);

void ones(int *matrix, int size);

double nmr_energy(struct nmr_noe *spect, struct mol_vector3 *grad, const double weight);

void nmr_grad_peaks_numeric(struct nmr_noe *spec, struct mol_atom_group *ag, const double step);

void nmr_grad_noe_energy_numeric(struct nmr_noe *spect, struct mol_atom_group *ag, const double step);

#endif /* _NMRGRAD_NOE_H_ */
