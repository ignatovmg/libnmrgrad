#include "noe.h"


static void _free_if_not_null(void *ar)
{
        if (ar != NULL) {
                free(ar);
        }
}


static int _number_of_atoms_in_lines(char *path)
{
        FILE *f = fopen(path, "r");
        if (f == NULL) {
                fprintf(stderr, "File not found\n");
                exit(EXIT_FAILURE);
        }

        char str[100];
        FILE *grp_file = fopen(path, "r");
        char *chunk;
        int natoms = 0;
        while (fgets(str, 100, grp_file) != NULL) {
                chunk = strtok(str, "  \t");
                while (chunk != NULL) {
                        natoms++;
                        chunk = strtok(NULL, " \t");
                }
        }

        fclose(grp_file);
        return natoms;
}


static int _number_of_lines(char *path)
{
        FILE *f = fopen(path, "r");
        if (f == NULL) {
                fprintf(stderr, "File not found\n");
                exit(EXIT_FAILURE);
        }
        char str[100];
        int i = 0;
        while (fgets(str, 100, f) != NULL)
                i++;
        fclose(f);

        return i;
}


void nmr_rx_mat(struct nmr_noe *spect, const struct mol_atom_group *ag)
{
        struct nmr_group_list *grps = spect->grps;
        double *rx = spect->rx;
        double omega = spect->omega;
        double t_cor = spect->t_cor;

        int size = grps->ngroups;
        int loc, loc1;
        int natoms = grps->natoms;

        if (rx == NULL || grps == NULL) {
                fprintf(stderr, "NULL pointer in %s\n", __func__);
                exit(EXIT_FAILURE);
        }

        double *gradx;
        double *grady;
        double *gradz;
        struct nmr_grad *grad = spect->rx_grad;

        if (grad != NULL) {
                gradx = grad->gradx;
                grady = grad->grady;
                gradz = grad->gradz;

                memset(gradx, 0, grad->natoms * size * size * sizeof(double));
                memset(grady, 0, grad->natoms * size * size * sizeof(double));
                memset(gradz, 0, grad->natoms * size * size * sizeof(double));
        }

        double J_0 = t_cor;
        double J_1 = t_cor / (1.0 + 4.0 * (M_PI * M_PI) * omega * omega * t_cor * t_cor);  // *10^(-9) s
        double J_2 = t_cor / (1.0 + 16.0 * (M_PI * M_PI) * omega * omega * t_cor * t_cor);  // *10^(-9) s

        double gyr = 2.6751965; // e+8
        double hbar = 1.0545919; // e-34;
        double coef = pow(gyr, 4) * pow(hbar, 2);

        double S0 = J_0 * coef; // 1/s
        double S1 = 1.5 * J_1 * coef; // 1/s
        double S2 = 6.0 * J_2 * coef; // 1/s

        struct nmr_group *grpi, *grpj;
        struct mol_vector3 *coordi, *coordj;
        double r2;
        double mult, mult1;

        int atomi = 0, atomj;
        int ptr1;//, ptr_rx, ptr_rx_t;
        double counter;
        double rx_val;
        double *r2_ptr = spect->r2;

        for (int i = 0; i < size; i++) {
                grpi = &grps->groups[i];
                atomj = atomi;
                for (int j = i; j < size; j++) {
                        grpj = &grps->groups[j];

                        counter = 0.0;
                        rx_val = 0.0;

                        if (i == j) {
                                // Self relaxation for unresolved groups if i == j
                                if (grpi->size > 1) {
                                        for (int i1 = 0; i1 < grpi->size; i1++) {
                                                ptr1 = natoms * (atomi + i1);
                                                for (int j1 = i1 + 1; j1 < grpj->size; j1++) {
                                                        /*r2 = (coordi->X - coordj->X) * (coordi->X - coordj->X) + \
                                                             (coordi->Y - coordj->Y) * (coordi->Y - coordj->Y) + \
                                                             (coordi->Z - coordj->Z) * (coordi->Z - coordj->Z);*/

                                                        r2 = r2_ptr[ptr1 + atomj + j1];

                                                        if (r2 > __DMIN__) {
                                                                rx_val += 1.0 / (r2 * r2 * r2);
                                                                counter += 1.0;

                                                                // accumulate gradient
                                                                if (grad != NULL) {
                                                                        double one_over_r8 = 1.0 / (r2 * r2 * r2 * r2);

                                                                        coordi = &ag->coords[grpi->group[i1]];
                                                                        coordj = &ag->coords[grpj->group[j1]];

                                                                        loc = (atomi + i1) * size * size +
                                                                              (i * size + j);
                                                                        gradx[loc] -= 6 * (coordi->X - coordj->X) *
                                                                                      one_over_r8;
                                                                        grady[loc] -= 6 * (coordi->Y - coordj->Y) *
                                                                                      one_over_r8;
                                                                        gradz[loc] -= 6 * (coordi->Z - coordj->Z) *
                                                                                      one_over_r8;

                                                                        loc = (atomj + j1) * size * size +
                                                                              (i * size + j);
                                                                        gradx[loc] -= 6 * (coordj->X - coordi->X) *
                                                                                      one_over_r8;
                                                                        grady[loc] -= 6 * (coordj->Y - coordi->Y) *
                                                                                      one_over_r8;
                                                                        gradz[loc] -= 6 * (coordj->Z - coordi->Z) *
                                                                                      one_over_r8;
                                                                }
                                                        }
                                                }
                                        }

                                        //rx[npeaks*i+j] /= counter;
                                        //rx[npeaks*i+j] *= 2 * (grpi->size - 1) * (S1 + S2);
                                        rx_val *= 2 * (grpi->size - 1) * (S1 + S2) / counter;

                                        // times self relaxation
                                        if (grad != NULL) {
                                                for (int i1 = 0; i1 < grpi->size; i1++) {
                                                        loc = (atomi + i1) * size * size + (i * size + j);
                                                        gradx[loc] *= 2 * (grpi->size - 1) * (S1 + S2) / counter;
                                                        grady[loc] *= 2 * (grpi->size - 1) * (S1 + S2) / counter;
                                                        gradz[loc] *= 2 * (grpi->size - 1) * (S1 + S2) / counter;

                                                }
                                        }
                                }
                        } else {
                                // 6 average if i != j
                                for (int i1 = 0; i1 < grpi->size; i1++) {
                                        ptr1 = natoms * (atomi + i1);
                                        for (int j1 = 0; j1 < grpj->size; j1++) {
                                                /*r2 = (coordi->X - coordj->X) * (coordi->X - coordj->X) + \
                                                     (coordi->Y - coordj->Y) * (coordi->Y - coordj->Y) + \
                                                     (coordi->Z - coordj->Z) * (coordi->Z - coordj->Z);*/

                                                r2 = r2_ptr[ptr1 + atomj + j1];

                                                if (r2 > __DMIN__) {
                                                        //rx[npeaks*i+j] += 1.0 / (r2 * r2 * r2);
                                                        rx_val += 1.0 / (r2 * r2 * r2);
                                                        counter += 1.0;

                                                        // accumulate gradient
                                                        if (grad != NULL) {
                                                                coordi = &ag->coords[grpi->group[i1]];
                                                                coordj = &ag->coords[grpj->group[j1]];

                                                                double one_over_r8 = 1.0 / (r2 * r2 * r2 * r2);

                                                                loc = (atomi + i1) * size * size + (i * size + j);
                                                                gradx[loc] -= 6 * (coordi->X - coordj->X) * one_over_r8;
                                                                grady[loc] -= 6 * (coordi->Y - coordj->Y) * one_over_r8;
                                                                gradz[loc] -= 6 * (coordi->Z - coordj->Z) * one_over_r8;

                                                                loc = (atomj + j1) * size * size + (i * size + j);
                                                                gradx[loc] -= 6 * (coordj->X - coordi->X) * one_over_r8;
                                                                grady[loc] -= 6 * (coordj->Y - coordi->Y) * one_over_r8;
                                                                gradz[loc] -= 6 * (coordj->Z - coordi->Z) * one_over_r8;
                                                        }
                                                }
                                        }
                                }

                                rx_val /= counter;

                                if (grad != NULL) {
                                        for (int i1 = 0; i1 < grpi->size; i1++) {
                                                loc = (atomi + i1) * size * size + (i * size + j);
                                                gradx[loc] /= counter;
                                                grady[loc] /= counter;
                                                gradz[loc] /= counter;
                                        }

                                        for (int j1 = 0; j1 < grpj->size; j1++) {
                                                loc = (atomj + j1) * size * size + (i * size + j);
                                                gradx[loc] /= counter;
                                                grady[loc] /= counter;
                                                gradz[loc] /= counter;
                                        }
                                }
                        }
                        rx[size * i + j] = rx_val;
                        rx[size * j + i] = rx_val;

                        atomj += grpj->size;
                }

                atomi += grpi->size;
        }


        // Fill diagonal elements in R
        mult = S0 + 2 * S1 + S2;
        double diag_val;
        for (int i = 0; i < size; i++) {
                diag_val = 0.0;
                for (int j = 0; j < size; j++) {
                        if (i != j) {
                                diag_val += mult * grps->groups[j].size * rx[i * size + j];
                        }
                }
                rx[i * size + i] += diag_val;
        }

        // Gradient
        if (grad != NULL) {
                for (int k = 0; k < grps->natoms; k++) {
                        loc = k * size * size;
                        for (int i = 0; i < size; i++) {
                                for (int j = 0; j < size; j++) {
                                        if (i != j) {
                                                mult1 = mult * grps->groups[j].size;

                                                if (i < j) {
                                                        loc1 = (i * size + j);
                                                } else {
                                                        loc1 = (j * size + i);
                                                }

                                                gradx[loc + (i * size + i)] += mult1 * gradx[loc + loc1];
                                                grady[loc + (i * size + i)] += mult1 * grady[loc + loc1];
                                                gradz[loc + (i * size + i)] += mult1 * gradz[loc + loc1];
                                        }
                                }
                        }
                }
        }

        // Fill the rest of R
        mult = S2 - S0;
        for (int i = 0; i < size - 1; i++) {
                for (int j = i + 1; j < size; j++) {
                        rx[i * size + j] = mult * sqrt(grps->groups[i].size * grps->groups[j].size) * rx[i * size + j];
                        rx[j * size + i] = rx[i * size + j];
                }
        }

        // Gradient
        if (grad != NULL) {
                int loc2;
                for (int k = 0; k < grps->natoms; k++) {
                        loc = k * size * size;
                        for (int i = 0; i < size - 1; i++) {
                                for (int j = i + 1; j < size; j++) {
                                        mult1 = mult * sqrt(grps->groups[i].size * grps->groups[j].size);

                                        loc1 = i * size + j;
                                        loc2 = j * size + i;
                                        gradx[loc + loc1] *= mult1;
                                        grady[loc + loc1] *= mult1;
                                        gradz[loc + loc1] *= mult1;

                                        gradx[loc + loc2] = gradx[loc + loc1];
                                        grady[loc + loc2] = grady[loc + loc1];
                                        gradz[loc + loc2] = gradz[loc + loc1];
                                }
                        }
                }
        }
}


struct nmr_workspace *nmr_workspace_create(int size)
{
        struct nmr_workspace *space = malloc(sizeof(struct nmr_workspace));
        nmr_workspace_init(space, size);
        return space;
}


void nmr_workspace_init(struct nmr_workspace *space, int size)
{
        space->size = size;
        space->eval = gsl_vector_alloc(size);
        space->evec = gsl_matrix_alloc(size, size);
        space->w = gsl_eigen_symmv_alloc(size);
        space->expeval = calloc(size, sizeof(double));
        space->pks2 = calloc(size * size, sizeof(double));
}


void nmr_workspace_destroy(struct nmr_workspace *space)
{
        if (space != NULL) {
                _free_if_not_null(space->eval);
                space->eval = NULL;
                _free_if_not_null(space->evec);
                space->evec = NULL;
                _free_if_not_null(space->w);
                space->w = NULL;
                _free_if_not_null(space->expeval);
                space->expeval = NULL;
                _free_if_not_null(space->pks2);
                space->pks2 = NULL;
        }
}


void nmr_workspace_free(struct nmr_workspace *space)
{
        nmr_workspace_destroy(space);
        _free_if_not_null(space);
}


void nmr_matrix_exponent(struct nmr_noe *spect)
{
        double *in = spect->in;
        double *rx = spect->rx;
        int *mask = spect->mask;

        struct nmr_grad *in_grad = spect->in_grad;
        struct nmr_grad *rx_grad = spect->rx_grad;

        int size = spect->size;
        double t_mix = spect->t_mix;

        if (in == NULL || rx == NULL) {
                fprintf(stderr, "NULL pointer in %s\n", __func__);
                exit(EXIT_FAILURE);
        }

        gsl_matrix_view m = gsl_matrix_view_array(rx, size, size);
        gsl_vector *eval = spect->space->eval;
        gsl_matrix *evec = spect->space->evec;
        gsl_eigen_symmv_workspace *w = spect->space->w;
        gsl_eigen_symmv(&m.matrix, eval, evec, w);

        double *expeval = spect->space->expeval;
        double *evals = eval->data;
        for (int i = 0; i < size; i++) {
                expeval[i] = exp(-t_mix * evals[i]);
                evals[i] *= -t_mix;
        }

        double *evecs = evec->data;
        double in_val;
        for (int i = 0; i < size; i++) {
                for (int j = i; j < size; j++) {
                        in_val = 0.0;
                        if (mask == NULL || mask[i * size + j] == 1) {
                                for (int k = 0; k < size; k++) {
                                        in_val += expeval[k] * evecs[i * size + k] * evecs[j * size + k];
                                }
                        }

                        in[i * size + j] = in_val;
                        in[j * size + i] = in_val;
                }
        }

        if (rx_grad != NULL && in_grad != NULL) {
                double *helperx = calloc(size * size, sizeof(double));
                double *helpery = calloc(size * size, sizeof(double));
                double *helperz = calloc(size * size, sizeof(double));

                double rux, ruy, ruz, ijx, ijy, ijz, c1, c2, c3;
                int loc;

                int atm = 0;
                for (int grp_i = 0; grp_i < spect->grps->ngroups; grp_i++) {
                        for (int grp_j = 0; grp_j < spect->grps->groups[grp_i].size; grp_j++) {
                                size_t atm_size_size = atm * size * size, grad_id2, grad_id;

                                for (int r = 0; r < size; r++) {
                                        for (int u = r; u < size; u++) { // TODO: check more carefully if we can start with u = r
                                                rux = 0.0;
                                                ruy = 0.0;
                                                ruz = 0.0;

                                                if (r != u) {
                                                        c1 = -(expeval[r] - expeval[u]) / (evals[r] - evals[u]) * t_mix;
                                                } else {
                                                        c1 = -expeval[r] * t_mix;
                                                }

                                                // Gradient of relaxation matrix is sparse. E.g. group_id = 2:
                                                //
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //     x  x  x  x  x  x  x  x
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //     0. 0. x  0. 0. 0. 0. 0.
                                                //
                                                // Therefore using here only non-zero values
                                                for (int s = 0; s < size; s++) {
                                                        c2 = c1 * evec->data[s * size + r];
                                                        grad_id2 = atm_size_size + s * size;
                                                        int t;
                                                        if (s != grp_i) {
                                                                t = grp_i;
                                                                c3 = c2 * evec->data[t * size + u];
                                                                grad_id = grad_id2 + t;
                                                                rux += c3 * rx_grad->gradx[grad_id];
                                                                ruy += c3 * rx_grad->grady[grad_id];
                                                                ruz += c3 * rx_grad->gradz[grad_id];

                                                                t = s;
                                                                c3 = c2 * evec->data[t * size + u];
                                                                grad_id = grad_id2 + t;
                                                                rux += c3 * rx_grad->gradx[grad_id];
                                                                ruy += c3 * rx_grad->grady[grad_id];
                                                                ruz += c3 * rx_grad->gradz[grad_id];
                                                        } else {
                                                                for (t = 0; t < size; t++) {
                                                                        c3 = c2 * evec->data[t * size + u];
                                                                        grad_id = grad_id2 + t;
                                                                        rux += c3 * rx_grad->gradx[grad_id];
                                                                        ruy += c3 * rx_grad->grady[grad_id];
                                                                        ruz += c3 * rx_grad->gradz[grad_id];
                                                                }
                                                        }
                                                }

                                                // We are going to use only the upper triangle of helper,
                                                // since its symmetrical
                                                loc = r * size + u;
                                                helperx[loc] = rux;
                                                helpery[loc] = ruy;
                                                helperz[loc] = ruz;

                                        }
                                }

                                int r_size_plus_u;
                                double evec_i_size_plus_r, evec_j_size_plus_u, evec_i_size_plus_u, evec_j_size_plus_r, evec_sum;
                                for (int i = 0; i < size; i++) {
                                        for (int j = i; j < size; j++) {
                                                ijx = 0.0;
                                                ijy = 0.0;
                                                ijz = 0.0;

                                                if (mask == NULL || mask[i * size + j] == 1) {
                                                        for (int r = 0; r < size; r++) {
                                                                evec_i_size_plus_r = evec->data[i * size + r];
                                                                evec_j_size_plus_r = evec->data[j * size + r];

                                                                // Start u with r, because of the helper matrix symmetry
                                                                for (int u = r; u < size; u++) {
                                                                        evec_i_size_plus_u = evec->data[i * size + u];
                                                                        evec_j_size_plus_u = evec->data[j * size + u];

                                                                        if (u == r) {
                                                                                evec_sum = evec_i_size_plus_r *
                                                                                           evec_j_size_plus_u;
                                                                        } else {
                                                                                evec_sum = (evec_i_size_plus_r *
                                                                                            evec_j_size_plus_u +
                                                                                            evec_j_size_plus_r *
                                                                                            evec_i_size_plus_u);
                                                                        }

                                                                        r_size_plus_u = r * size + u;
                                                                        ijx += helperx[r_size_plus_u] * evec_sum;
                                                                        ijy += helpery[r_size_plus_u] * evec_sum;
                                                                        ijz += helperz[r_size_plus_u] * evec_sum;
                                                                }
                                                        }
                                                }

                                                loc = atm_size_size + i * size + j;
                                                //printf("%i (%i, %i): %f %f %f\n", atm, i, j, ijx, ijy, ijz);
                                                in_grad->gradx[loc] = ijx;
                                                in_grad->grady[loc] = ijy;
                                                in_grad->gradz[loc] = ijz;

                                                loc = atm_size_size + j * size + i;
                                                in_grad->gradx[loc] = ijx;
                                                in_grad->grady[loc] = ijy;
                                                in_grad->gradz[loc] = ijz;
                                        }
                                }
                                atm++;
                        }
                }

                free(helperx);
                free(helpery);
                free(helperz);
        }
}


void nmr_compute_peaks(struct nmr_noe *spect, const struct mol_atom_group *ag)
{
        double *in = spect->in;
        struct nmr_group_list *grps = spect->grps;

        int size = grps->ngroups;

        // Relaxation matrix
        nmr_rx_mat(spect, ag);

        // Intensities
        nmr_matrix_exponent(spect);

        // Normalization
        for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                        in[i * size + j] *= sqrt(grps->groups[i].size * grps->groups[j].size);
                }
        }

        if (spect->in_grad != NULL) {
                int idx = 0;
                double *gradx = spect->in_grad->gradx;
                double *grady = spect->in_grad->grady;
                double *gradz = spect->in_grad->gradz;
                double coef;

                for (int k = 0; k < grps->natoms; k++) {
                        for (int i = 0; i < size; i++) {
                                for (int j = 0; j < size; j++) {
                                        coef = sqrt(grps->groups[i].size * grps->groups[j].size);
                                        gradx[idx] *= coef;
                                        grady[idx] *= coef;
                                        gradz[idx] *= coef;

                                        idx++;
                                }
                        }
                }
        }
}


void nmr_compute_peaks_no_grad(struct nmr_noe *spect, const struct mol_atom_group *ag)
{
        struct nmr_grad *rx_ana = spect->rx_grad;
        struct nmr_grad *in_ana = spect->in_grad;
        spect->rx_grad = NULL;
        spect->in_grad = NULL;

        nmr_compute_peaks(spect, ag);

        spect->rx_grad = rx_ana;
        spect->in_grad = in_ana;
}


void nmr_r2_mat(double *r2, const struct mol_atom_group *ag, const struct nmr_group_list *grps)
{
        int size = grps->natoms;

        struct mol_vector3 *crdi, *crdj;

        for (int i = 0; i < size; i++) {
                crdi = &ag->coords[grps->atoms[i]];

                for (int j = i; j < size; j++) {
                        crdj = &ag->coords[grps->atoms[j]];

                        r2[i * size + j] = (crdj->X - crdi->X) * (crdj->X - crdi->X) + \
                           (crdj->Y - crdi->Y) * (crdj->Y - crdi->Y) + \
                           (crdj->Z - crdi->Z) * (crdj->Z - crdi->Z);

                        r2[j * size + i] = r2[i * size + j];
                }
        }
}


double *r2_mat_recalc(double *r2, struct mol_atom_group *ag, struct nmr_group_list *grps, int atom)
{
        int size = grps->natoms;

        struct mol_vector3 *crdi, *crdj;
        crdi = &ag->coords[grps->atoms[atom]];
        for (int j = 0; j < size; j++) {
                crdj = &ag->coords[grps->atoms[j]];

                r2[atom * size + j] = (crdj->X - crdi->X) * (crdj->X - crdi->X) + \
                          (crdj->Y - crdi->Y) * (crdj->Y - crdi->Y) + \
                          (crdj->Z - crdi->Z) * (crdj->Z - crdi->Z);

                r2[j * size + atom] = r2[atom * size + j];
        }

        return r2;
}

void nmr_grad_peaks_numeric(struct nmr_noe *spec, struct mol_atom_group *ag, const double step)
{
        struct nmr_group_list *grps = spec->grps;
        int size = grps->ngroups;
        int msize = size * size;

        // save gradients pointers
        struct nmr_grad *in_grad = spec->in_grad;
        struct nmr_grad *rx_grad = spec->rx_grad;

        // make NULL so they will not be computed analytically
        spec->in_grad = NULL;
        spec->rx_grad = NULL;

        nmr_r2_mat(spec->r2, ag, spec->grps);

        nmr_compute_peaks(spec, ag);
        double *pks1 = spec->in;

        spec->in = spec->space->pks2;
        double *pks2;
        double one_over_step = 1.0 / step;
        double *gradx = in_grad->gradx;
        double *grady = in_grad->grady;
        double *gradz = in_grad->gradz;

        struct nmr_group grp;
        struct mol_vector3 *coords;
        int atom, i, j, k;
        int counter = 0;
        int counter2 = 0;
        double oldcrd;
        for (i = 0; i < size; i++) {
                grp = grps->groups[i];
                for (j = 0; j < grp.size; j++) {
                        atom = grp.group[j];
                        coords = &ag->coords[atom];

                        // fill grad for X
                        oldcrd = coords->X;
                        coords->X += step;
                        r2_mat_recalc(spec->r2, ag, grps, counter2);

                        nmr_compute_peaks(spec, ag);
                        pks2 = spec->in;

                        coords->X = oldcrd;
                        r2_mat_recalc(spec->r2, ag, grps, counter2);

                        for (k = 0; k < msize; k++) {
                                gradx[counter + k] = (pks2[k] - pks1[k]) * one_over_step;
                        }

                        // fill grad for Y
                        oldcrd = coords->Y;
                        coords->Y += step;
                        r2_mat_recalc(spec->r2, ag, grps, counter2);
                        nmr_compute_peaks(spec, ag);
                        pks2 = spec->in;

                        coords->Y = oldcrd;
                        r2_mat_recalc(spec->r2, ag, grps, counter2);

                        for (k = 0; k < msize; k++) {
                                grady[counter + k] = (pks2[k] - pks1[k]) * one_over_step;
                        }

                        // fill grad for Z
                        oldcrd = coords->Z;
                        coords->Z += step;
                        r2_mat_recalc(spec->r2, ag, grps, counter2);
                        nmr_compute_peaks(spec, ag);
                        pks2 = spec->in;

                        coords->Z = oldcrd;
                        r2_mat_recalc(spec->r2, ag, grps, counter2);

                        for (k = 0; k < msize; k++) {
                                gradz[counter + k] = (pks2[k] - pks1[k]) * one_over_step;
                        }

                        counter += msize;
                        counter2++;
                }
        }

        //free(pks2);
        spec->in = pks1;
        spec->in_grad = in_grad;
        spec->rx_grad = rx_grad;
}


void nmr_grad_noe_energy_numeric(struct nmr_noe *spect, struct mol_atom_group *ag, const double step)
{
        struct nmr_group_list *grps = spect->grps;
        int size = grps->ngroups;

        // save gradients pointers
        struct nmr_grad *in_grad = spect->in_grad;
        struct nmr_grad *rx_grad = spect->rx_grad;

        // make NULL so they will not be computed analytically
        spect->in_grad = NULL;
        spect->rx_grad = NULL;

        nmr_r2_mat(spect->r2, ag, spect->grps);
        nmr_compute_peaks(spect, ag);
        double init_energy = nmr_energy(spect, NULL, 1.0);
        double one_over_step = 1.0 / step;

        struct nmr_group grp;
        struct mol_vector3 *coords;
        int atom, i, j;
        int atomi = 0;
        double oldcrd;
        for (i = 0; i < size; i++) {
                grp = grps->groups[i];
                for (j = 0; j < grp.size; j++) {
                        atom = grp.group[j];
                        coords = &ag->coords[atom];

                        // fill grad for X
                        oldcrd = coords->X;
                        coords->X += step;
                        r2_mat_recalc(spect->r2, ag, grps, atomi);
                        nmr_compute_peaks(spect, ag);
                        double new_energy = nmr_energy(spect, NULL, 1.0);

                        coords->X = oldcrd;
                        r2_mat_recalc(spect->r2, ag, grps, atomi);
                        ag->gradients[atom].X = (new_energy - init_energy) * one_over_step;

                        // fill grad for Y
                        oldcrd = coords->Y;
                        coords->Y += step;
                        r2_mat_recalc(spect->r2, ag, grps, atomi);
                        nmr_compute_peaks(spect, ag);
                        new_energy = nmr_energy(spect, NULL, 1.0);

                        coords->Y = oldcrd;
                        r2_mat_recalc(spect->r2, ag, grps, atomi);
                        ag->gradients[atom].Y = (new_energy - init_energy) * one_over_step;

                        // fill grad for Z
                        oldcrd = coords->Z;
                        coords->Z += step;
                        r2_mat_recalc(spect->r2, ag, grps, atomi);
                        nmr_compute_peaks(spect, ag);
                        new_energy = nmr_energy(spect, NULL, 1.0);

                        coords->Z = oldcrd;
                        r2_mat_recalc(spect->r2, ag, grps, atomi);
                        ag->gradients[atom].Z = (new_energy - init_energy) * one_over_step;

                        atomi++;
                }
        }

        spect->in_grad = in_grad;
        spect->rx_grad = rx_grad;
}


double *grad_numeric_rx(struct nmr_noe *spect, struct mol_atom_group *ag)
{
        struct nmr_group_list *grps = spect->grps;
        int size = grps->ngroups;

        struct nmr_grad *rx_grad = spect->rx_grad;
        struct nmr_grad *in_grad = spect->in_grad;
        spect->in_grad = NULL;
        spect->rx_grad = NULL;

        nmr_rx_mat(spect, ag);
        double *rx1 = calloc(size * size, sizeof(double));
        memcpy(rx1, spect->rx, size * size * sizeof(double));

        double *rx2;

        double step = 1E-9;
        double *grad = calloc(3 * grps->natoms * size * size, sizeof(double));

        int counter = 0;

        for (int i = 0; i < size; i++) {
                for (int j = 0; j < grps->groups[i].size; j++) {
                        int atom = grps->groups[i].group[j];

                        // fill grad for X
                        ag->coords[atom].X += step;
                        nmr_rx_mat(spect, ag);
                        rx2 = spect->rx;
                        ag->coords[atom].X -= step;

                        for (int k = 0; k < size * size; k++) {
                                grad[0 * grps->natoms * size * size + counter * size * size + k] =
                                        (rx2[k] - rx1[k]) / step;
                        }

                        // fill grad for Y
                        ag->coords[atom].Y += step;
                        nmr_rx_mat(spect, ag);
                        rx2 = spect->rx;
                        ag->coords[atom].Y -= step;

                        for (int k = 0; k < size * size; k++) {
                                grad[1 * grps->natoms * size * size + counter * size * size + k] =
                                        (rx2[k] - rx1[k]) / step;
                        }

                        // fill grad for Z
                        ag->coords[atom].Z += step;
                        nmr_rx_mat(spect, ag);
                        rx2 = spect->rx;
                        ag->coords[atom].Z -= step;

                        for (int k = 0; k < size * size; k++) {
                                grad[2 * grps->natoms * size * size + counter * size * size + k] =
                                        (rx2[k] - rx1[k]) / step;
                        }

                        counter++;
                }
        }

        free(rx1);
        //free(rx2);

        spect->in_grad = in_grad;
        spect->rx_grad = rx_grad;

        return grad;
}

double *nmr_matrix_read(const char *path, const int size, int *mask)
{
        //int nlines = _number_of_lines(path);
        //if (nlines != size * size) {
        //        _PRINTF_ERROR("%s should have %i lines (found %i instead)\n", path, size * size, nlines)
        //}

        FILE *f = fopen(path, "r");
        double *mat = calloc(size * size, sizeof(double));

        int i, j, nchar;
        double val;
        while ((nchar = fscanf(f, "%i %i %lf", &i, &j, &val)) != EOF) {
                if (nchar != 3) {
                        _PRINTF_ERROR("Wrong line format");
                }

                if (i >= size) {
                        _PRINTF_ERROR("Index exceeds the original matrix size (%i >= %i)", i, size);
                }

                if (j >= size) {
                        _PRINTF_ERROR("Index exceeds the original matrix size (%i >= %i)", j, size);
                }

                mat[i * size + j] = val;
                //mat[j*size+i] = val;

                if (mask != NULL) {
                        mask[i * size + j] = 1;
                }
        }

        fclose(f);

        return mat;
}


double nmr_energy(struct nmr_noe *spect, struct mol_vector3 *grad, const double weight)
{
        int natoms = spect->grps->natoms;
        int ngroups = spect->size;
        double *exp = spect->exp;
        double *cmp = spect->in;
        int *mask = spect->mask;
        double *gradx;
        double *grady;
        double *gradz;
        int *atoms;
        struct mol_vector3 grd;
        struct mol_vector3 *k_derivs;

        if (exp == NULL || cmp == NULL) {
                fprintf(stderr, "NULL pointer in %s\n", __func__);
                exit(EXIT_FAILURE);
        }

        if (grad != NULL) {
                gradx = spect->in_grad->gradx;
                grady = spect->in_grad->grady;
                gradz = spect->in_grad->gradz;
                atoms = spect->grps->atoms;
                k_derivs = calloc(natoms, sizeof(struct mol_vector3));
        }

        double power = 1.0 / 6.0;
        double in_o, sn_o, in_o_pow, in_c_pow, sn_c, in_c, delta, coef;
        double k_num = 0.0, k_den = 0.0;

        for (int i = 0; i < ngroups; i++) {
                for (int j = i + 1; j < ngroups; j++) {
                        if (mask == NULL || mask[i * ngroups + j] == 1) {
                                in_o = exp[i * ngroups + j];
                                //sn_o = in_o > 0.0 ? 1 : -1; // not fabs(in_o) / in_o, because in_o can be zero
                                //in_o_pow = sn_o * pow(fabs(in_o), power);
                                in_o_pow = pow(fabs(in_o), power);

                                in_c = cmp[i * ngroups + j];
                                //sn_c = in_c > 0.0 ? 1 : -1;
                                //in_c_pow = sn_c * pow(fabs(in_c), power);
                                in_c_pow = pow(fabs(in_c), power);

                                k_num += in_c_pow * in_o_pow;
                                k_den += in_o_pow * in_o_pow;

                                if (grad != NULL && in_c != 0. && in_o != 0.) {
                                        coef = power * in_o_pow * in_c_pow / in_c;
                                        for (int atm = 0; atm < natoms; atm++) {
                                                // gradient
                                                int index = atm * ngroups * ngroups + i * ngroups + j;
                                                grd.X = coef * gradx[index];
                                                grd.Y = coef * grady[index];
                                                grd.Z = coef * gradz[index];
                                                MOL_VEC_ADD(k_derivs[atm], k_derivs[atm], grd);
                                        }
                                }
                        }
                }
        }

        if (k_den == 0.) {
                _PRINTF_ERROR("All observed intensities are zero");
        }

        double ene = 0.0;
        double best_k = k_num / k_den;
        double c1, c2;
        for (int i = 0; i < ngroups; i++) {
                for (int j = i + 1; j < ngroups; j++) {
                        if (mask == NULL || mask[i * ngroups + j] == 1) {
                                in_o = exp[i * ngroups + j];
                                //sn_o = in_o > 0.0 ? 1 : -1;
                                //in_o_pow = sn_o * pow(fabs(in_o), power);
                                in_o_pow = pow(fabs(in_o), power);


                                in_c = cmp[i * ngroups + j];
                                //sn_c = in_c > 0.0 ? 1 : -1;
                                //in_c_pow = sn_c * pow(fabs(in_c), power);
                                in_c_pow = pow(fabs(in_c), power);

                                delta = (in_c_pow - best_k * in_o_pow);
                                ene += weight * delta * delta;

                                if (grad != NULL && delta != 0.0) {
                                        c1 = weight * 2. * delta;
                                        c2 = (in_c != 0.) ? (power * in_c_pow / in_c) : 0.;
                                        for (int atm = 0; atm < natoms; atm++) {
                                                // gradient
                                                int index = atm * ngroups * ngroups + i * ngroups + j;
                                                grd.X = c1 *
                                                        (c2 * gradx[index] - k_derivs[atm].X * in_o_pow / k_den);
                                                grd.Y = c1 *
                                                        (c2 * grady[index] - k_derivs[atm].Y * in_o_pow / k_den);
                                                grd.Z = c1 *
                                                        (c2 * gradz[index] - k_derivs[atm].Z * in_o_pow / k_den);
                                                MOL_VEC_SUB(grad[atoms[atm]], grad[atoms[atm]], grd);
                                        }
                                }
                        }
                }
        }

        if (grad != NULL) {
                free(k_derivs);
        }

        spect->energy = ene;
        spect->scale = best_k;
        return ene;
}


int *get_mask(double *exp, double cutoff, int size)
{
        int *mask = calloc(size * size, sizeof(int));

        for (int i = 0; i < size; i++) {
                for (int j = i; j < size; j++) {
                        if (fabs(exp[i * size + j]) > __DMIN__) {
                                mask[i * size + j] = 1;
                                mask[j * size + i] = 1;
                        } else {
                                mask[i * size + j] = 0;
                                mask[j * size + i] = 0;
                        }
                }
        }

        return mask;
}


void ones(int *matrix, int size)
{
        for (int i = 0; i < size; i++) {
                for (int j = i; j < size; j++) {
                        matrix[i * size + j] = 1;
                        matrix[j * size + i] = 1;
                }
        }
}


struct nmr_noe *nmr_noe_create(struct nmr_group_list *grps, double omega, double t_cor, double t_mix, double cutoff)
{
        struct nmr_noe *spec = calloc(1, sizeof(struct nmr_noe));
        nmr_noe_init(spec, grps, omega, t_cor, t_mix, cutoff);
        return spec;
}


void nmr_noe_init(struct nmr_noe *spec,
                  struct nmr_group_list *grps,
                  double omega,
                  double t_cor,
                  double t_mix,
                  double cutoff)
{
        // Read proton groups
        if (grps == NULL) {
                _PRINTF_ERROR("Groups cannot be NULL");
        }
        spec->grps = grps;

        int size = spec->grps->ngroups;
        spec->size = size;
        spec->in = calloc(size * size, sizeof(double));
        spec->rx = calloc(size * size, sizeof(double));
        spec->r2 = calloc(spec->grps->natoms * spec->grps->natoms, sizeof(double));
        spec->space = nmr_workspace_create(size);

        spec->omega = omega; // GHz
        spec->t_cor = t_cor; // ns
        spec->t_mix = t_mix; // s
        spec->cutoff = cutoff;

        spec->mask = NULL;
        spec->exp = NULL;
        spec->in_grad = NULL;
        spec->rx_grad = NULL;
}


void nmr_noe_destroy(struct nmr_noe *spec)
{
        if (spec != NULL) {
                nmr_group_list_free(spec->grps);
                spec->grps = NULL;
                nmr_workspace_free(spec->space);
                spec->space = NULL;
                nmr_grad_free(spec->in_grad);
                spec->in_grad = NULL;
                nmr_grad_free(spec->rx_grad);
                spec->rx_grad = NULL;

                _free_if_not_null(spec->in);
                spec->in = NULL;
                _free_if_not_null(spec->rx);
                spec->rx = NULL;
                _free_if_not_null(spec->r2);
                spec->r2 = NULL;
                _free_if_not_null(spec->exp);
                spec->exp = NULL;
                _free_if_not_null(spec->mask);
                spec->mask = NULL;
        }
}


void nmr_noe_free(struct nmr_noe *spec)
{
        nmr_noe_destroy(spec);
        _free_if_not_null(spec);
}


struct nmr_group_list *nmr_group_list_create(int ngroups, int natoms)
{
        struct nmr_group_list *grps = calloc(1, sizeof(struct nmr_group_list));
        nmr_group_list_init(grps, ngroups, natoms);
        return grps;
}


void nmr_group_list_init(struct nmr_group_list *grps, int ngroups, int natoms)
{
        grps->ngroups = ngroups;
        grps->groups = calloc(ngroups, sizeof(struct nmr_group));

        grps->natoms = natoms;
        grps->atoms = calloc(natoms, sizeof(int));
}


void nmr_group_list_destroy(struct nmr_group_list *grps)
{
        if (grps != NULL) {
                _free_if_not_null(grps->groups);
                grps->groups = NULL;
                _free_if_not_null(grps->atoms);
                grps->atoms = NULL;
        }
}


void nmr_group_list_free(struct nmr_group_list *grps)
{
        nmr_group_list_destroy(grps);
        _free_if_not_null(grps);
}


struct nmr_group_list *nmr_group_list_read(char *path)
{
        struct nmr_group_list *grps = nmr_group_list_create(_number_of_lines(path), _number_of_atoms_in_lines(path));

        FILE *grp_file = fopen(path, "r");
        char str[100];
        char *chunk;
        int group_id = 0;
        int natoms = 0;
        while (fgets(str, 100, grp_file) != NULL) {
                int grp_size = 0;
                grps->groups[group_id].id = group_id;
                chunk = strtok(str, "  \t");
                while (chunk != NULL) {
                        if (grp_size == 6) {
                                fprintf(stderr, "Group cant me larger than 6\n");
                                exit(EXIT_FAILURE);
                        }

                        int atom_id = atoi(chunk) - 1;
                        grps->groups[group_id].group[grp_size] = atom_id;
                        grps->atoms[natoms] = atom_id;
                        grp_size++;
                        natoms++;
                        chunk = strtok(NULL, " \t");
                }

                grps->groups[group_id].size = grp_size;
                group_id++;
        }

        fclose(grp_file);
        return grps;
}


void nmr_group_list_write(const char *path, const struct nmr_group_list *groups)
{
        FILE *f = fopen(path, "w");
        nmr_group_list_fwrite(f, groups);
        fclose(f);
}


void nmr_group_list_fwrite(FILE *f, const struct nmr_group_list *groups)
{
        for (int i = 0; i < groups->ngroups; i++) {
                fprintf(f, "group %i of npeaks %i: ", groups->groups[i].id, groups->groups[i].size);
                for (int j = 0; j < groups->groups[i].size; j++) {
                        fprintf(f, "%i\t", groups->groups[i].group[j]);
                }
                fprintf(f, "\n");
        }
}


struct nmr_grad *nmr_grad_create(int natoms, int npeaks)
{
        struct nmr_grad *grad = calloc(1, sizeof(struct nmr_grad));
        nmr_grad_init(grad, natoms, npeaks);
        return grad;
}


void nmr_grad_init(struct nmr_grad *grad, int natoms, int npeaks)
{
        grad->size = natoms * npeaks * npeaks;
        grad->natoms = natoms;
        grad->npeaks = npeaks;

        grad->gradx = calloc(grad->size, sizeof(double));
        grad->grady = calloc(grad->size, sizeof(double));
        grad->gradz = calloc(grad->size, sizeof(double));
}


void nmr_grad_destroy(struct nmr_grad *grad)
{
        if (grad != NULL) {
                _free_if_not_null(grad->gradx);
                grad->gradx = NULL;
                _free_if_not_null(grad->grady);
                grad->grady = NULL;
                _free_if_not_null(grad->gradz);
                grad->gradz = NULL;
        }
}


void nmr_grad_free(struct nmr_grad *grad)
{
        nmr_grad_destroy(grad);
        _free_if_not_null(grad);
}


void nmr_matrix_fwrite(FILE *f, const double *m, const int size)
{
        for (int i = 0; i < size; i++) {
                for (int j = 0; j < size - 1; j++)
                        fprintf(f, "%.6f\t", m[i * size + j]);
                fprintf(f, "%.6f\n", m[i * size + size - 1]);
        }
}

void nmr_matrix_fwrite_stacked(FILE *f, const double *m, const int size)
{
        for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                        fprintf(f, "%i\t%i\t%.6f\n", i, j, m[i * size + j]);
                }
        }
}

void nmr_matrix_write(const char *path, const double *m, const int size)
{
        FILE *file = fopen(path, "w");
        nmr_matrix_fwrite(file, m, size);
        fclose(file);
}
