#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>
#include <time.h>

#include "mol2/pdb.h"
#include "noe.h"


static void _compare_two_matrices(const double *comp, const double *real, const int size)
{
        for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                        ck_assert_double_eq_tol(comp[i * size + j], real[i * size + j], 0.00001);
                }
        }
}


static void _compare_gradients(struct nmr_grad *num, struct nmr_grad *ana)
{

        ck_assert_int_eq(num->natoms, ana->natoms);
        ck_assert_int_eq(num->npeaks, ana->npeaks);

        for (int i = 0; i < num->natoms * num->npeaks * num->npeaks; i++) {
                ck_assert_double_eq_tol(num->gradx[i], ana->gradx[i], 0.0001);
                ck_assert_double_eq_tol(num->grady[i], ana->grady[i], 0.0001);
                ck_assert_double_eq_tol(num->gradz[i], ana->gradz[i], 0.0001);
        }
}


static void _test_gradient(char *groups, char *pdb)
{
        struct nmr_group_list *g = nmr_group_list_read(groups);
        struct mol_atom_group *ag = mol_read_pdb(pdb);
        struct nmr_noe *spec = nmr_noe_create(g, 0.00006, 0.1, 0.1, 10.);

        struct nmr_grad *in_ana = nmr_grad_create(ag->natoms, g->ngroups);
        struct nmr_grad *rx_ana = nmr_grad_create(ag->natoms, g->ngroups);
        spec->rx_grad = rx_ana;
        spec->in_grad = in_ana;
        nmr_r2_mat(spec->r2, ag, spec->grps);
        nmr_compute_peaks(spec, ag);

        struct nmr_grad *in_num = nmr_grad_create(ag->natoms, g->ngroups);
        struct nmr_grad *rx_num = nmr_grad_create(ag->natoms, g->ngroups);
        spec->rx_grad = rx_num;
        spec->in_grad = in_num;
        nmr_grad_peaks_numeric(spec, ag, 0.0000001);

        _compare_gradients(in_num, in_ana);

        nmr_noe_free(spec);
        mol_atom_group_free(ag);
        nmr_grad_free(in_ana);
        nmr_grad_free(rx_ana);
}


static void _test_noe_energy_grad(char *groups, char *pdb, char *exp_path)
{
        //printf("%s\n", pdb);
        struct nmr_group_list *g = nmr_group_list_read(groups);
        struct mol_atom_group *ag = mol_read_pdb(pdb);
        struct nmr_noe *spec = nmr_noe_create(g, 0.00006, 0.1, 0.2, 1000.);

        struct nmr_grad *in_ana = nmr_grad_create(ag->natoms, g->ngroups);
        struct nmr_grad *rx_ana = nmr_grad_create(ag->natoms, g->ngroups);
        spec->rx_grad = rx_ana;
        spec->in_grad = in_ana;
        nmr_r2_mat(spec->r2, ag, spec->grps);
        nmr_compute_peaks(spec, ag);
        //nmr_matrix_fwrite(stdout, spec->in, spec->size);

        spec->exp = nmr_matrix_read(exp_path, g->ngroups, NULL);
        struct mol_vector3 *grad_ana = calloc(ag->natoms, sizeof(struct mol_vector3));
        nmr_energy(spec, grad_ana, 1.0);

        struct mol_vector3 *grad_num = calloc(ag->natoms, sizeof(struct mol_vector3));
        ag->gradients = grad_num;
        nmr_grad_noe_energy_numeric(spec, ag, 0.000001);

        for (int i = 0; i < ag->natoms; i++) {
                //printf("%f %f\n", grad_ana[i].X, grad_num[i].X);
                ck_assert_double_eq_tol(grad_ana[i].X, -grad_num[i].X, 0.0001);
                ck_assert_double_eq_tol(grad_ana[i].Y, -grad_num[i].Y, 0.0001);
                ck_assert_double_eq_tol(grad_ana[i].Z, -grad_num[i].Z, 0.0001);
        }

        nmr_noe_free(spec);
        mol_atom_group_free(ag);
        free(grad_ana);
}


START_TEST(nmr_test_2h_peaks)
        {
                struct nmr_group_list *g = nmr_group_list_read("2h.groups");
                struct mol_atom_group *ag = mol_read_pdb("2h.pdb");
                struct nmr_noe *spec = nmr_noe_create(g, 0.00006, 0.1, 0.2, 10);

                nmr_r2_mat(spec->r2, ag, spec->grps);
                nmr_compute_peaks(spec, ag);
                double *real = nmr_matrix_read("2h.mat", spec->size, NULL);
                _compare_two_matrices(spec->in, real, spec->size);

                nmr_noe_free(spec);
                mol_atom_group_free(ag);
                free(real);
        }END_TEST


START_TEST(nmr_test_3h_peaks)
        {
                struct nmr_group_list *g = nmr_group_list_read("3h.groups");
                struct mol_atom_group *ag = mol_read_pdb("3h.pdb");
                struct nmr_noe *spec = nmr_noe_create(g, 0.00006, 0.1, 0.2, 10);

                nmr_r2_mat(spec->r2, ag, spec->grps);
                nmr_compute_peaks(spec, ag);
                double *real = nmr_matrix_read("3h.mat", spec->size, NULL);
                _compare_two_matrices(spec->in, real, spec->size);

                nmr_noe_free(spec);
                mol_atom_group_free(ag);
                free(real);
        }END_TEST


START_TEST(nmr_test_6h_peaks)
        {
                struct nmr_group_list *g = nmr_group_list_read("6h.groups");
                struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
                struct nmr_noe *spec = nmr_noe_create(g, 0.00006, 0.1, 0.2, 10);

        nmr_r2_mat(spec->r2, ag, spec->grps);
        nmr_compute_peaks(spec, ag);
                double *real = nmr_matrix_read("6h.mat", spec->size, NULL);
        _compare_two_matrices(spec->in, real, spec->size);

        nmr_noe_free(spec);
        mol_atom_group_free(ag);
        free(real);
        }END_TEST


START_TEST(nmr_test_3h_gradient)
        {
        _test_gradient("3h.groups", "3h.pdb");
        }END_TEST


START_TEST(nmr_test_6h_gradient)
        {
        _test_gradient("6h.groups", "6h.pdb");
        }END_TEST


START_TEST(nmr_test_mol_gradient)
        {
        _test_gradient("mol.groups", "mol.pdb");
        }END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_self)
        {
        _test_noe_energy_grad("6h.groups", "6h.pdb", "6h.mat");
        }END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_perturbed)
        {
        _test_noe_energy_grad("6h.groups", "6h.pdb", "6h_perturbed.mat");
        }END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_cil)
        {
        _test_noe_energy_grad("cil_md_ch22.groups", "cil_md_ch22.pdb", "cil_md_ch22.mat");
        }END_TEST


START_TEST(nmr_test_grad_ana_time)
        {
                struct nmr_group_list *g = nmr_group_list_read("cil_md_ch22.groups");
                struct mol_atom_group *ag = mol_read_pdb("cil_md_ch22.pdb");
                struct nmr_noe *spec = nmr_noe_create(g, 0.00006, 0.1, 0.3, 10);

                struct nmr_grad *in_ana = nmr_grad_create(ag->natoms, g->ngroups);
                struct nmr_grad *rx_ana = nmr_grad_create(ag->natoms, g->ngroups);
                spec->rx_grad = rx_ana;
                spec->in_grad = in_ana;

                //int* mask = calloc(g->ngroups * g->ngroups, sizeof(double));
                spec->exp = nmr_matrix_read("cil_md_ch22.mat", g->ngroups, NULL);

                //nmr_matrix_fwrite(stdout, spec->in, spec->size);

                clock_t start, end;
                double cpu_time_used;

                start = clock();
                struct mol_vector3 *grad_ana = calloc(ag->natoms, sizeof(struct mol_vector3));
                for (int i = 0; i < 100; i++) {
                        nmr_r2_mat(spec->r2, ag, spec->grps);
                        nmr_compute_peaks(spec, ag);
                        nmr_energy(spec, grad_ana, 1.0);
                }
                end = clock();
                cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
                printf("%f\n", cpu_time_used);

                struct mol_vector3 *grad_num = calloc(ag->natoms, sizeof(struct mol_vector3));
                ag->gradients = grad_num;
                start = clock();
                for (int i = 0; i < 100; i++) {
                        nmr_grad_noe_energy_numeric(spec, ag, 1E-8);
                }
                end = clock();
                cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
                printf("%f\n", cpu_time_used);

        }END_TEST



Suite *lists_suite(void)
{
        Suite *suite = suite_create("nmr_grad");

        TCase *tcase_peaks = tcase_create("peaks");
        //tcase_add_checked_fixture(tcase_real, setup_real, teardown_real);
        tcase_add_test(tcase_peaks, nmr_test_2h_peaks);
        tcase_add_test(tcase_peaks, nmr_test_3h_peaks);
        tcase_add_test(tcase_peaks, nmr_test_6h_peaks);
        suite_add_tcase(suite, tcase_peaks);

        TCase *tcase_grads = tcase_create("gradients");
        tcase_add_test(tcase_grads, nmr_test_3h_gradient);
        tcase_add_test(tcase_grads, nmr_test_6h_gradient);
        tcase_add_test(tcase_grads, nmr_test_mol_gradient);
        tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_self);
        tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_perturbed);
        tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_cil);
        //tcase_add_test(tcase_grads, nmr_test_grad_ana_time);
        //tcase_set_timeout(tcase_grads, 200);
        suite_add_tcase(suite, tcase_grads);

        return suite;
}


int main(void)
{
        Suite *suite = lists_suite();
        SRunner *runner = srunner_create(suite);
        srunner_run_all(runner, CK_ENV);

        int number_failed = srunner_ntests_failed(runner);
        srunner_free(runner);
        return number_failed;
}