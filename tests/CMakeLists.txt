include_directories(${CHECK_INCLUDE_DIRS})
link_directories(${CHECK_LIBRARY_DIRS})

set(tests
        first_test)

# Configure data files
file(GLOB test_files "${CMAKE_CURRENT_SOURCE_DIR}/data/*")
foreach(filepath ${test_files})
    get_filename_component(filename ${filepath} NAME)
    configure_file(${filepath} ${CMAKE_CURRENT_BINARY_DIR}/${filename} COPYONLY)
endforeach()

set(CMAKE_EXE_LINKER_FLAGS "${CHECK_LDFLAGS_OTHER} ${CMAKE_EXE_LINKER_FLAGS}")
# Add and register tests
foreach(test ${tests})
    add_executable(${test} ${test}.c)

    target_link_libraries(${test}
            mol2
            nmrgrad
            ${CHECK_LIBRARIES}
            m)
    add_test(${test} ${CMAKE_CURRENT_BINARY_DIR}/${test})
endforeach()
